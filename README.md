# Project 1 - Laravel

Full Stack Web Development - PKS Digital School Batch 2.

## Kelompok 5

- [Sudiono](https://gitlab.com/sudiono "Gitlab Page")
- [Pandu Wijaya](https://gitlab.com/vancisme "Gitlab Page")
- [Nosef Nunggaran](https://gitlab.com/nnunggaran "Gitlab Page")

## Tema Project

Film Review

## ERD

![ERD of this project](/ERD.png)

## Links

- Demo Aplikasi: [LINK](https://gitlab.com/sudiono/kelompok5laravel)
- Deploy Website: [LINK](https://gitlab.com/sudiono/kelompok5laravel)
